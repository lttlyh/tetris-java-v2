package org.liu;

/**
 * Created by emliu on 2015/5/15.
 */
public class Entity {
    public int width;
    public int height;
    public int x = 0;
    public int y = 0;
    public int[][] data;
    public int flag = FLAG_NORMAL;
    public int color;

    public static final int FLAG_NORMAL = 0;
    public static final int FLAG_STICK = 1;
    public static final int FLAG_SQUARE = 2;

    public int get(int x, int y) {
        return data[y][x];
    }

    public void set(int x, int y, int val) {
        data[y][x] = val;
    }

    public static void rotate(Entity e) {
        if(e.flag == FLAG_STICK) {
            if(e.data[0][1] == 1) {
                for(int i = 0; i < 4; ++i) e.data[i][1] = 0;
                for(int i = 0; i < 4; ++i) e.data[2][i] = 1;
            } else {
                for (int i = 0; i < 4; ++i) e.data[2][i] = 0;
                for (int i = 0; i < 4; ++i) e.data[i][1] = 1;
            }
        } else if(e.flag == FLAG_SQUARE) {
            return;
        } else {
            int oldWidth = e.width;
            int oldHeight = e.height;
            e.width = oldHeight;
            e.height = oldWidth;
            int[][] newData = new int[e.height][e.width];
            for (int y = 0; y < oldHeight; ++y) {
                for (int x = 0; x < oldWidth; ++x) {
                    newData[x][oldHeight - 1 - y] = e.data[y][x];
                }
            }
            e.data = newData;
        }
    }

    public static Entity dup(Entity entity) {
        Entity e = new Entity();
        e.width = entity.width;
        e.height = entity.height;
        e.x = entity.x;
        e.y = entity.y;
        e.data = dupArray(entity.data);
        e.flag = entity.flag;
        e.color = entity.color;
        return e;
    }

    private static int[][] dupArray(int[][] arr) {
        int[][] ret = new int[arr.length][];
        for(int i = 0; i < arr.length; ++i) {
            ret[i] = new int[arr[i].length];
            System.arraycopy(arr[i], 0, ret[i], 0, arr[i].length);
        }
        return ret;
    }

    public static Entity genRandomEntity() {
        int i = (int)Math.floor(Math.random() * 7);
        Entity e = new Entity();
        e.color = (int)Math.floor(Math.random() * 7) + 1;
        if(i == 0) { // i
            e.width = raw_i_width;
            e.height = raw_i_height;
            e.data = dupArray(raw_i);
            e.flag = FLAG_STICK;
        } else if(i == 1) { // l
            e.width = raw_l_width;
            e.height = raw_l_height;
            e.data = dupArray(raw_l);
        } else if(i == 2) { // j
            e.width = raw_j_width;
            e.height = raw_j_height;
            e.data = dupArray(raw_j);
        } else if(i == 3) { // s
            e.width = raw_s_width;
            e.height = raw_s_height;
            e.data = dupArray(raw_s);
        } else if(i == 4) { // z
            e.width = raw_z_width;
            e.height = raw_z_height;
            e.data = dupArray(raw_z);
        } else if(i == 5) { // t
            e.width = raw_t_width;
            e.height = raw_t_height;
            e.data = dupArray(raw_t);
        } else { // o
            e.width = raw_o_width;
            e.height = raw_o_height;
            e.data = dupArray(raw_o);
            e.flag = FLAG_SQUARE;
        }
        for(int j = 0; j < (int)Math.floor(Math.random() * 4); ++j) {
            Entity.rotate(e);
        }
        return e;
    }

    private static int[][] raw_i = new int[][] {
            {0, 1, 0, 0},
            {0, 1, 0, 0},
            {0, 1, 0, 0},
            {0, 1, 0, 0}
    };
    private static int raw_i_width = 4;
    private static int raw_i_height = 4;

    private static int[][] raw_l = new int[][] {
            {0, 1, 0},
            {0, 1, 0},
            {0, 1, 1},
    };
    private static int raw_l_width = 3;
    private static int raw_l_height = 3;

    private static int[][] raw_j = new int[][] {
            {0, 1, 0},
            {0, 1, 0},
            {1, 1, 0},
    };
    private static int raw_j_width = 3;
    private static int raw_j_height = 3;

    private static int[][] raw_s = new int[][] {
            {0, 1, 1},
            {1, 1, 0},
            {0, 0, 0}
    };
    private static int raw_s_width = 3;
    private static int raw_s_height = 3;

    private static int[][] raw_z = new int[][] {
            {1, 1, 0},
            {0, 1, 1},
            {0, 0, 0}
    };
    private static int raw_z_width = 3;
    private static int raw_z_height = 3;

    private static int [][] raw_t = new int[][] {
            {0, 1, 0},
            {1, 1, 1},
            {0, 0, 0}
    };
    private static int raw_t_width = 3;
    private static int raw_t_height = 3;

    private static int[][] raw_o = new int[][] {
            {1, 1},
            {1, 1}
    };
    private static int raw_o_width = 2;
    private static int raw_o_height = 2;
}
