package org.liu;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

/**
 * Created by liu on 15-5-20.
 */
public class Drawer extends JPanel {

    private Main main;
    private Image buffer;

    public void setMain(Main main) {
        this.main = main;
    }

    public Drawer() {
        super();
        setPreferredSize(new Dimension(400, 300));
        setMinimumSize(new Dimension(400, 300));
        buffer = createImage(getWidth(), getHeight());
        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                buffer = createImage(getWidth(), getHeight());
            }
        });
    }

    public static void main(String[] args) {
        JFrame f = new JFrame("Hello world!!!");
        f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        f.setSize(400, 300);;
        f.getContentPane().add(new Drawer());
        f.pack();
        f.setVisible(true);
    }

    @Override
    public void paintComponent(Graphics g) {

     }
}