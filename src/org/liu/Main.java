package org.liu;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;

/**
 * Created by emliu on 2015/5/15.
 */
public class Main {
    public static final int BLOCK_NONE = 0;
    public static final int BLOCK_CYAN = 1;
    public static final int BLOCK_BLUE_MAGENTA = 2;
    public static final int BLOCK_MAGENTA = 3;
    public static final int BLOCK_LIGHT_GREY = 4;
    public static final int BLOCK_LIME = 5;
    public static final int BLOCK_YELLOW = 6;
    public static final int BLOCK_RED = 7;

    public static final Color COLOR_CYAN = new Color(0, 255, 255);
    public static final Color COLOR_BLUE_MAGENTA = new Color(128, 0, 255);
    public static final Color COLOR_MAGENTA = new Color(255, 0, 255);
    public static final Color COLOR_LIGHT_GREY = new Color(100, 100, 100);
    public static final Color COLOR_LIME = new Color(128, 255, 0);
    public static final Color COLOR_YELLOW = new Color(255, 255, 0);
    public static final Color COLOR_RED = new Color(255, 0, 0);
    public static final Color COLOR_BACKGROUND = new Color(100, 100, 100);
    public static final Color COLOR_FOREGROUND = new Color(200, 200, 200);

    public boolean inGame = false;
    public boolean printGameOver = false;
    public Ground ground = new Ground();
    public Entity currEntity = null;
    public Entity nextEntity = null;
    public Timer timer = new Timer(this);
    public OldDrawer drawer;
    public int score = 0;

    public static void main(String[] args) {
        Main main = new Main();
        OldDrawer drawer = new OldDrawer(main);
        main.reset();
        main.inGame = false;
        main.drawer = drawer;
        drawer.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        drawer.addKeyListener(new UserKeyListener(main));
        drawer.setVisible(true);
    }

    public void reset() {
        inGame = true;
        printGameOver = false;
        ground.clear();
        currEntity = Entity.genRandomEntity();
        currEntity.x = 9;
        currEntity.y = 0;
        nextEntity = Entity.genRandomEntity();
        score = 0;
        timer.start();
        if(drawer != null)
            drawer.repaintGround();
    }

    public void gamerOver() {
        timer.stop();
        inGame = false;
        printGameOver = true;
    }

    private boolean PlACE_FLAG = false;
    public void tick() {
        if(PlACE_FLAG) {
            shiftEntity();
            PlACE_FLAG = false;
        } else {
            ++currEntity.y;
            if (ground.collision(currEntity)) {
                --currEntity.y;
                ground.place(currEntity);
                int collapse = ground.collapse();
                if(collapse > 0)
                    score += (int)Math.pow(2, collapse);
                PlACE_FLAG = true;
//                if(collapse > 0)
//                    drawer.repaintGround();
//                return;
            }
//            drawer.repaintEntity(currEntity, 0);
        }
        drawer.repaint();
    }

    public void space() {
        reset();
    }

    public void up() {
        Entity e = Entity.dup(currEntity);
        Entity.rotate(e);
        if(!ground.collision(e)) {
            currEntity = e;
        }
//        drawer.repaintEntity(currEntity, 2);
        drawer.repaint();
    }

    public void right() {
        ++currEntity.x;
        if(ground.collision(currEntity)) --currEntity.x;
//        drawer.repaintEntity(currEntity, 3);
        drawer.repaint();
    }

    public void left() {
        --currEntity.x;
        if(ground.collision(currEntity)) ++currEntity.x;
//        drawer.repaintEntity(currEntity, 1);
        drawer.repaint();
    }

    public void downPressed() {
        timer.setFast();
    }

    public void downReleased() {
        timer.setNormal();
    }

    private void shiftEntity() {
        nextEntity.x = 9;
        nextEntity.y = 0;
        if(ground.collision(nextEntity)) {
            gamerOver();
            return;
        }
        currEntity = nextEntity;
        nextEntity = Entity.genRandomEntity();
    }
}
