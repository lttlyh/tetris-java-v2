package org.liu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by emliu on 2015/5/15.
 */
public class Timer {
    public static final int STEP_NORMAL = 1;
    public static final int STEP_FAST = 11;
    public static final int COUNT_MAX = 10;
    public static final int ATOM_MS = 30;

    private int count = 0;
    boolean fastFlag = false;
    private Main main;
    ActionListener listener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            count += fastFlag ? STEP_FAST : STEP_NORMAL;
            if(count > COUNT_MAX) {
                main.tick();
                count = 0;
            }
        }
    };
    javax.swing.Timer timer = new javax.swing.Timer(ATOM_MS, listener);

    public Timer(Main main) {
        this.main = main;
    }

    public void start() {
        timer.start();
    }

    public void setFast() {
        this.fastFlag = true;
    }

    public void setNormal() {
        this.fastFlag = false;
    }

    public void stop() {
        timer.stop();
    }
}
