package org.liu;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Created by emliu on 2015/5/15.
 */
public class UserKeyListener implements KeyListener {

    public static final char KEY_UP = 'w';
    public static final char KEY_RIGHT = 'd';
    public static final char KEY_LEFT = 'a';
    public static final char KEY_SPACE = ' ';
    public static final int KEY_DOWN_CODE = KeyEvent.VK_S;

    private Main main;

    public UserKeyListener(Main main) {
        this.main = main;
    }

    @Override
    public void keyTyped(KeyEvent e) {
        if(main.inGame) {
            if (e.getKeyChar() == KEY_UP) {
                main.up();
            } else if (e.getKeyChar() == KEY_LEFT) {
                main.left();
            } else if (e.getKeyChar() == KEY_RIGHT) {
                main.right();
            }
        }else if(e.getKeyChar() == KEY_SPACE) {
            main.space();
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KEY_DOWN_CODE) {
            main.downPressed();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if(e.getKeyCode() == KEY_DOWN_CODE) {
            main.downReleased();
        }
    }
}
